process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';
const express = require("express");
const cors = require('cors');
if (process.env.API_ENV === 'PROD') {
  require('dotenv').config({ path: '.env.prod' })
} else {
  require('dotenv').config({ path: '.env' })
}
const sequelize = require('./lib/db');

const app = express();
app.use(express.json());
app.use(cors());


const RouterManager = require("./routes");
RouterManager(app);

sequelize.sync().then(() => console.log('connected to DB'))
app.listen(process.env.PORT || 4000), () => console.log("listening on port 4000");
